<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth', 'admin'])->group(function () {
 Route::get('/', [HomeController::class, 'index'])->name('home');
 Route::get('/detailTaskTeam/{id}', [HomeController::class, 'show'])->name('taskTeamDetail');
 Route::get('/detailTeamUsers/{id}', [HomeController::class, 'showTeamUsers'])->name('detailTeamUsers');
 Route::get('/users', [UserController::class, 'index'])->name('users');
 Route::get('/tasks', [TaskController::class, 'index'])->name('tasks');
 Route::get('/tasks/edit/{id}', [TaskController::class, 'edit'])->name('editTask');
 Route::get('/teams/edit/{id}', [TeamController::class, 'edit'])->name('editTeam');
 Route::get('/teams', [TeamController::class, 'index'])->name('teams');
 Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});
