<?php

use App\Http\Controllers\Api\V1\TaskController;
use App\Http\Controllers\Api\V1\TeamController;
use App\Http\Controllers\Api\V1\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['cors'])->group(function () {
    Route::apiResource('users', UserController::class);
    Route::apiResource('tasks', TaskController::class);
    Route::apiResource('teams', TeamController::class);
    Route::post('/teams/asignarUserToTeam', [TeamController::class, 'asignarUserToTeam']);
    Route::post('/tasks/asignarTeamToTask', [TaskController::class, 'asignarTeamToTask']);
    Route::delete('/teams/delete-relation/{id}/{idTeam}', [TeamController::class, 'deleteRelationUserToTeam']);
    Route::delete('/tasks/delete-relation/{id}/{idTask}', [TaskController::class, 'deleteRelationTeamToTask']);
    Route::put('/teams/to-assign/{idTeam}', [TeamController::class, 'toAssignLeaderUser']);
    Route::post('/tasks/edit-task', [TaskController::class, 'updateTask']);
});
