export const swalDelete = (text, callback) => {
  Swal.fire({
    title: "¿Estas seguro de eliminar?",
    text: text,
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Yes, delete it!",
  }).then((result) => {
    if (result.isConfirmed) {
      callback();
    }
  });
};

export const swalAlert = (bnd, msg) => {
  if (bnd) {
    Swal.fire(msg, "exito!!");
    setTimeout(() => {
      location.reload();
    }, 1000);
  } else {
    Swal.fire(msg, "error");
  }
};
