const { swalAlert, swalDelete } = require("../messages/messages");

$(document).ready(() => {
  $.ajaxSetup({
    headers: {
      Authorization: "Bearer " + $("meta[name=api-token]").attr("content"),
    },
  });
  $("#tableUsers").DataTable();
  let idTeam = 0;
  $("#btnAddTeams").click(() => {
    $("#teamsModal").modal("show");
  });

  $(".btnClose").click(() => {
    $("#teamsModal").modal("hide");
    $("#usersToTeamsModal").modal("hide");
  });

  $(".btnAsignarUser").click((e) => {
    $("#usersToTeamsModal").modal("show");
    idTeam = e.target.value;
  });

  //DELETE
  $(".btnDelete").click((e) => {
    let id = e.target.value;
    swalDelete("Eliminara este equipo definitivamente", () => {
      $.ajax({
        method: "DELETE",
        url: `/api/teams/${id}`,
      }).done(function (msg) {
        swalAlert("Eliminado", msg.msg);
      });
    });
  });

  //ADD
  $("#formTeams").submit((e) => {
    e.preventDefault();
    let data = $("#formTeams").serialize();
    $.ajax({
      method: "POST",
      url: `/api/teams`,
      data: data,
    }).done(function (res) {
      let msg = res.msg;
      let html = "";
      var errors = [];
      if (res.success) {
        html = msg;
      } else {
        $.each(msg, function (idx2, val2) {
          errors.push(val2);
        });
        html = errors.join(", ");
      }
      swalAlert(res.success, html);
    });
  });

  //ASIGNAR USERS
  $(".checkAsignacion").click((e) => {
    e.preventDefault();
    console.log("team" + idTeam);
    console.log(e.target.value);
    $.ajax({
      method: "POST",
      url: `/api/teams/asignarUserToTeam`,
      data: { team_id: idTeam, user_id: e.target.value },
    }).done(function (res) {
      let msg = res.msg;
      let html = "";
      var errors = [];
      if (res.success) {
        html = msg;
      } else {
        $.each(msg, function (idx2, val2) {
          errors.push(val2);
        });
        html = errors.join(", ");
      }
      swalAlert(res.success, html);
    });
  });

  //UPDATE
  $("#formTeamsEdit").submit((e) => {
    e.preventDefault();
    let data = $("#formTeamsEdit").serialize();
    let id = $("#btnFormTeamsEdit").val();
    $.ajax({
      method: "PUT",
      url: `/api/teams/${id}`,
      data: data,
    }).done(function (res) {
      let msg = res.msg;
      let html = "";
      var errors = [];
      if (res.success) {
        html = msg;
      } else {
        $.each(msg, function (idx2, val2) {
          errors.push(val2);
        });
        html = errors.join(", ");
      }
      swalAlert(res.success, html);
    });
  });

  //Delete Relation with users
  $(".btnDeleteRelation").click((e) => {
    e.preventDefault();
    let id = e.target.value;
    let idTeam = e.target.dataset.id;
    swalDelete("Eliminara este usuario del equipo", () => {
      $.ajax({
        method: "DELETE",
        url: `/api/teams/delete-relation/${id}/${idTeam}`,
      }).done(function (msg) {
        swalAlert("Eliminado", msg.msg);
      });
    });
  });

  //Assign leader
  $(".btnToAssignLeader").click((e) => {
    let idU = e.target.value;
    let idT = e.target.dataset.id;
    $.ajax({
      method: "PUT",
      url: `/api/teams/to-assign/${idT}`,
      data: { id: idU },
    }).done(function (res) {
      let msg = res.msg;
      let html = "";
      var errors = [];
      if (res.success) {
        html = msg;
      } else {
        $.each(msg, function (idx2, val2) {
          errors.push(val2);
        });
        html = errors.join(", ");
      }
      swalAlert(res.success, html);
    });
  });
});
