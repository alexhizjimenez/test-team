const { swalAlert, swalDelete } = require("../messages/messages");

$(document).ready(() => {
  $.ajaxSetup({
    headers: {
      Authorization: "Bearer " + $("meta[name=api-token]").attr("content"),
    },
  });
  $("#btnAddUsers").click(() => {
    $("#usersModal").modal("show");
  });

  $(".btnClose").click(() => {
    $("#usersModal").modal("hide");
  });
  //DELETE
  $(".btnDelete").click((e) => {
    let id = e.target.value;
    swalDelete("Eliminara este usuario definitivamente", () => {
      $.ajax({
        method: "DELETE",
        url: `/api/users/${id}`,
      }).done(function (msg) {
        swalAlert("Eliminado", msg.msg);
      });
    });
  });

  //ADD
  $("#formUsers").submit((e) => {
    e.preventDefault();
    let data = $("#formUsers").serialize();
    $.ajax({
      method: "POST",
      url: `/api/users`,
      data: data,
    }).done(function (res) {
      let msg = res.msg;
      let html = "";
      var errors = [];
      if (res.success) {
        html = msg;
      } else {
        $.each(msg, function (idx2, val2) {
          errors.push(val2);
        });
        html = errors.join(", ");
      }
      swalAlert(res.success, html);
    });
  });
});
