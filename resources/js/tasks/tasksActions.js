const { swalAlert, swalDelete } = require("../messages/messages");

$(document).ready(() => {
  $.ajaxSetup({
    headers: {
      Authorization: "Bearer " + $("meta[name=api-token]").attr("content"),
    },
  });
  $("#tableTeams").DataTable();
  let idTask = 0;
  $("#btnAddTasks").click(() => {
    $("#tasksModal").modal("show");
  });

  $(".btnClose").click(() => {
    $("#tasksModal").modal("hide");
    $("#teamToTasksModal").modal("hide");
  });

  //DELETE
  $(".btnDelete").click((e) => {
    let id = e.target.value;
    swalDelete("Eliminara esta tarea definitivamente", () => {
      $.ajax({
        method: "DELETE",
        url: `/api/tasks/${id}`,
      }).done(function (msg) {
        swalAlert("Eliminado", msg.msg);
      });
    });
  });

  $(".btnAsignarTeam").click((e) => {
    $("#teamToTasksModal").modal("show");
    idTask = e.target.value;
  });

  //ADD
  $("#formTasks").submit((e) => {
    e.preventDefault();
    let fd = new FormData();
    fd.append("name", $("#name").val());
    fd.append("description", $("#description").val());
    fd.append("file", $("#file")[0].files[0]);
    $.ajax({
      method: "POST",
      url: `/api/tasks`,
      data: fd,
      cache: false,
      contentType: false,
      processData: false,
    }).done(function (res) {
      let msg = res.msg;
      let html = "";
      var errors = [];
      if (res.success) {
        html = msg;
      } else {
        $.each(msg, function (idx2, val2) {
          errors.push(val2);
        });
        html = errors.join(", ");
      }
      swalAlert(res.success, html);
    });
  });

  //asignar equipos
  $(".checkAsignacion").click((e) => {
    e.preventDefault();
    console.log("task" + idTask);
    console.log(e.target.value);
    $.ajax({
      method: "POST",
      url: `/api/tasks/asignarTeamToTask`,
      data: { task_id: idTask, team_id: e.target.value },
    }).done(function (res) {
      let msg = res.msg;
      let html = "";
      var errors = [];
      if (res.success) {
        html = msg;
      } else {
        $.each(msg, function (idx2, val2) {
          errors.push(val2);
        });
        html = errors.join(", ");
      }
      swalAlert(res.success, html);
    });
  });

  //UPDATE
  $("#formTasksEdit").submit((e) => {
    e.preventDefault();
    //let data = $("#formTasksEdit").serialize();
    let id = $("#btnFormTasksEdit").val();
    let fd = new FormData();
    fd.append("id", id);
    fd.append("name", $("#name").val());
    fd.append("description", $("#description").val());
    fd.append("file", $("#file")[0].files[0]);

    $.ajax({
      method: "POST",
      url: `/api/tasks/edit-task`,
      data: fd,
      cache: false,
      contentType: false,
      processData: false,
    }).done(function (res) {
      let msg = res.msg;
      let html = "";
      var errors = [];
      if (res.success) {
        html = msg;
      } else {
        $.each(msg, function (idx2, val2) {
          errors.push(val2);
        });
        html = errors.join(", ");
      }
      swalAlert(res.success, html);
    });
  });

  //Delete Relation with users
  $(".btnDeleteRelation").click((e) => {
    e.preventDefault();
    let id = e.target.value;
    let idTask = e.target.dataset.id;
    swalDelete("Eliminara este usuario del equipo", () => {
      $.ajax({
        method: "DELETE",
        url: `/api/tasks/delete-relation/${id}/${idTask}`,
      }).done(function (msg) {
        swalAlert("Eliminado", msg.msg);
      });
    });
  });
});
