@extends('layouts.app')
@section('title', 'TASKS')
@section('content')
    <div class="row">
        <div class="card col-md-12 my-2">
            <div class="card-body">
                <form id="formTeamsEdit">
                    <div class="form-group">
                        <label for="name" class="col-form-label">{{ __('front.name') }}</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{old('name',$team->name)}}">
                    </div>
                    <button type="submit" class="btn btn-primary my-2 col-md-12" id="btnFormTeamsEdit" value="{{$team->id}}">Guardar</button>
                </form>
                
                <a href="{{ route('teams') }}" class="btn btn-warning my-4 ">{{ __('front.back') }}</a>
            </div>
        </div>
        <h1>{{ __('front.users') }} : {{$team->name}}</h1>
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>{{ __('front.id') }}</th>
                    <th>{{ __('front.name') }}</th>
                    <th>{{ __('front.email') }}</th>
                    <th>{{ __('front.leader') }}</th>
                    <th>{{ __('front.actions') }}</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($team->users as $t )
                
                    <tr>
                        <td>{{$t->id}}</td>
                        <td>{{$t->name}}</td>
                        <td>{{$t->email}}</td>
                        <td>{{($t->pivot->leader) ? 'Lider':''}}</td>
                        <td><button class="btn btn-danger btnDeleteRelation" data-id="{{$t->pivot->team_id}}" value="{{$t->id}}">{{ __('front.deleteTeam') }}</button>
                            @if (!$t->pivot->leader)   
                            <button class="btn btn-success btnToAssignLeader" data-id="{{$t->pivot->team_id}}" value="{{$t->id}}">{{ __('front.assignLeader') }}</button></td>
                            @endif
                    </tr>
                @empty
                    <tr><td colspan="4">{{ __('front.noRows') }}</td></tr>
                @endforelse
            </tbody>
        </table>
    @endsection
    @section('scripts')
    <script src="{{ URL::ASSET('/js/teams/teamsActions.js') }}"></script>
    @endsection
