@extends('layouts.app')
@section('title','TASKS')
@section('content')
<button class="btn btn-primary" id="btnAddTasks">{{ __('front.add') }} {{ __('front.tasks') }}</button>
<div class="modal" id="tasksModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('front.add') }} {{ __('front.tasks') }}</h5>
                <button type="button" class="close btnClose" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formTasks" enctype="multipart/form-data" method="post">
                    <div class="form-group">
                        <label for="name" class="col-form-label">{{ __('front.name') }}</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-form-label">{{ __('front.description') }}</label>
                        <textarea name="description" id="description" class="form-control" cols="30" rows="10"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-form-label">{{ __('front.file') }}</label>
                        <input type="file" class="form-control" id="file" name="file">
                    </div>
                    
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btnClose" data-dismiss="modal">{{ __('front.cancel') }}</button>
                <button type="submit" class="btn btn-primary">{{ __('front.save') }}</button>
            </div>
            </form> 
        </div>
    </div>
</div>
<div class="row">
    <div class="table">
        <table class="table table-hover table-striped table-responsive" >
            <thead>
                <tr>
                    <th>{{ __('front.id') }}</th>
                    <th>{{ __('front.name') }}</th>
                    <th>{{ __('front.description') }}</th>
                    <th>{{ __('front.file') }}</th>
                    <th>{{ __('front.actions') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tasks as $t)
                    <tr>
                        <td>{{$t->id}}</td>
                        <td>{{$t->name}}</td>
                        <td>{{$t->description}}</td>
                        <td>{{($t->file != "")? 'Hay archivo': 'No hay archivo'}}</td>
                        <td><a href="{{route('editTask',$t->id)}}" class="btn btn-success m-auto">{{ __('front.edit') }}</a> <button class="btn btn-danger my-2 btnDelete" value="{{$t->id}}">{{ __('front.delete') }}</button>
                            <button class="btn btn-warning btnAsignarTeam" value="{{$t->id}}">{{ __('front.assignTeam') }}</button></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$tasks->links()}}
    </div>
</div>
<div class="modal" id="teamToTasksModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('front.teams') }}</h5>
                <button type="button" class="close btnClose" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table">
                    <table class="table table-hover table-striped table-responsive" id="tableTeams">
                        <thead>
                            <tr>
                                <th>{{ __('front.id') }}</th>
                                <th>{{ __('front.name') }}</th>
                                <th>{{ __('front.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($teams as $t)
                                <tr>
                                    <td>{{$t->id}}</td>
                                    <td>{{$t->name}}</td>
                                    <td><input type="checkbox" class=" checkAsignacion" value="{{$t->id}}"></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btnClose" data-dismiss="modal">{{ __('front.cancel') }}</button>
            </div>
            
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ URL::ASSET('/js/tasks/tasksActions.js') }}"></script>
@endsection
