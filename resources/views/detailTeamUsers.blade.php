@extends('layouts.app')
@section('title', 'TASKS')
@section('content')
    <div class="row">
        <div class="card col-md-12 my-2">
            <div class="card-body">
                <h5 class="card-title">{{ __('front.name') }} {{ $team->name }}</h5>
                <p>{{ __('front.users') }}</p>
                <ul>
                    @forelse ($team->users as $tu)
                        <li>{{$tu->name}} </li>
                    @empty
                        <li>{{ __('front.noRows') }}</li>
                    @endforelse
                </ul>
                <a href="{{ route('home') }}" class="btn btn-primary">{{ __('front.back') }}</a>
            </div>
        </div>
    @endsection
    @section('scripts')
    @endsection
