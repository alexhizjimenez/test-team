@extends('layouts.app')
@section('title', 'HOME')
@section('content')
    <div class="row">
        @forelse ($tasks as $t)
        <div class="card col-md-4 my-2" >
            <div class="card-body">
              <h5 class="card-title">Nombre: {{$t->name}}</h5>
              <p class="card-text">{{$t->description}}</p>
              <a href="{{route('taskTeamDetail',$t->id)}}" class="btn btn-primary">Ver detalle</a>
            </div>
          </div>
        @empty
            <h1>No hay tareas</h1>
        @endforelse
    </div>
@endsection
@section('scripts')

@endsection
