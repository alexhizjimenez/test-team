@extends('layouts.app')
@section('title', 'TASKS')
@section('content')
    <div class="row">
        <div class="card col-md-12 my-2">
            <div class="card-body">
                <form id="formTasksEdit" enctype="multipart/form-data"  method="post">
                    <div class="form-group">
                        <label for="name" class="col-form-label">{{ __('front.name') }}</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{old('name',$task->name)}}">
                    </div>
                    <div class="form-group">
                        <label for="members" class="col-form-label">{{ __('front.description') }}</label>
                        <textarea type="number" class="form-control" id="description" name="description" >{{old('description',$task->description)}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="file" class="col-form-label">{{ __('front.file') }}</label>
                      <input type="file" name="file" id="file" class="form-control">
                      @if ($task->file)
                        <p style="color: red; ">{{ __('front.fileExists') }}</p>
                    @endif
                    </div>
                    <button type="submit" class="btn btn-primary my-2 col-md-12" id="btnFormTasksEdit" value="{{$task->id}}">{{ __('front.save') }}</button>
                </form>
                
                <a href="{{ route('tasks') }}" class="btn btn-warning my-4 ">{{ __('front.back') }}</a>
            </div>
        </div>
        <h1>{{ __('front.teams') }}{{$task->name}}</h1>
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>{{ __('front.id') }}</th>
                    <th>{{ __('front.name') }}</th>
                    <th>{{ __('front.actions') }}</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($task->teams as $t )
                    <tr>
                        <td>{{$t->id}}</td>
                        <td>{{$t->name}}</td>
                        <td><button class="btn btn-danger btnDeleteRelation" data-id="{{$t->pivot->task_id}}" value="{{$t->id}}">Eliminar asignacion de equipo</button></td>
                    </tr>
                @empty
                    <tr><td colspan="4">{{ __('front.noRows') }}</td></tr>
                @endforelse
            </tbody>
        </table>
    @endsection
    @section('scripts')
    <script src="{{ URL::ASSET('/js/tasks/tasksActions.js') }}"></script>
    @endsection
