@extends('layouts.app')
@section('title', 'TEAMS')
@section('content')
@section('content')
    <button class="btn btn-primary" id="btnAddTeams">{{ __('front.add') }} {{ __('front.teams') }}</button>
    <div class="modal" id="teamsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('front.add') }}</h5>
                    <button type="button" class="close btnClose" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formTeams">
                        <div class="form-group">
                            <label for="name" class="col-form-label">{{ __('front.name') }}</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btnClose" data-dismiss="modal">{{ __('front.cancel') }}</button>
                    <button type="submit" class="btn btn-primary ">{{ __('front.save') }}</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="table">
            <table class="table table-hover table-striped table-responsive" >
                <thead>
                    <tr>
                        <th>{{ __('front.id') }}</th>
                        <th>{{ __('front.name') }}</th>
                        <th>{{ __('front.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($teams as $t)
                        <tr>
                            <td>{{ $t->id }}</td>
                            <td>{{ $t->name }}</td>
                            <td><a href="{{route('editTeam',$t->id)}}" class="btn btn-success m-auto">{{ __('front.edit') }}</a> <button
                                    class="btn btn-danger my-2 btnDelete" value="{{ $t->id }}">{{ __('front.delete') }}</button>
                                <button class="btn btn-warning btnAsignarUser" value="{{$t->id}}">{{ __('front.assignUsers') }}</button></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$teams->links()}}
        </div>
    </div>
    <div class="modal" id="usersToTeamsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('front.users') }}</h5>
                    <button type="button" class="close btnClose" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table">
                        <table class="table table-hover table-striped table-responsive" id="tableUsers">
                            <thead>
                                <tr>
                                    <th>{{ __('front.id') }}</th>
                                    <th>{{ __('front.name') }}</th>
                                    <th>{{ __('front.email') }}</th>
                                    <th>{{ __('front.phone') }}</th>
                                    <th>{{ __('front.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $u)
                                    <tr>
                                        <td>{{$u->id}}</td>
                                        <td>{{$u->name}}</td>
                                        <td>{{$u->email}}</td>
                                        <td>{{$u->phone}}</td>
                                        <td><input type="checkbox" class=" checkAsignacion" value="{{$u->id}}"></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btnClose" data-dismiss="modal">{{ __('front.cancel') }}</button>

                </div>
                
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ URL::ASSET('/js/teams/teamsActions.js') }}"></script>
@endsection
