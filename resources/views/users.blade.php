@extends('layouts.app')
@section('title', 'USERS')
@section('content')
    <button class="btn btn-primary" id="btnAddUsers">Agregar usuarios</button>
    <div class="modal" id="usersModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nuevo usuario</h5>
                    <button type="button" class="close btnClose" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formUsers">
                        <div class="form-group">
                            <label for="name" class="col-form-label">{{ __('front.name') }}</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-form-label">{{ __('front.email') }}</label>
                            <input type="email" class="form-control" id="email" name="email"></input>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="col-form-label">{{ __('front.phone') }}</label>
                            <input type="text" class="form-control" id="phone" name="phone">
                        </div>
                        <div class="form-group">
                            <label for="address" class="col-form-label">{{ __('front.address') }}</label>
                            <input type="text" class="form-control" id="address" name="address"></input>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btnClose" data-dismiss="modal">{{ __('front.cancel') }}</button>
                        <button type="submit" class="btn btn-primary">{{ __('front.save') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="table">
            <table class="table table-hover table-striped table-responsive" >
                <thead>
                    <tr>
                        <th>{{ __('front.id') }}</th>
                        <th>{{ __('front.name') }}</th>
                        <th>{{ __('front.email') }}</th>
                        <th>{{ __('front.phone') }}</th>
                        <th>{{ __('front.address') }}</th>
                        <th>{{ __('front.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $u)
                        <tr>
                            <td>{{$u->id}}</td>
                            <td>{{$u->name}}</td>
                            <td>{{$u->email}}</td>
                            <td>{{$u->phone}}</td>
                            <td>{{$u->address}}</td>
                            <td><button class="btn btn-danger my-2 btnDelete" value="{{$u->id}}">{{ __('front.delete') }}</button></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$users->links()}}
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ URL::ASSET('/js/users/usersActions.js') }}"></script>
@endsection
