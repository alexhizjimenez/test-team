@extends('layouts.app')
@section('title','TASKS')
@section('content')
<div class="row">
 <div class="card col-md-12 my-2" >
  <div class="card-body">
    <h5 class="card-title">{{ __('front.name') }} {{$task->name}}</h5>
    <p class="card-text">{{ __('front.description') }} {{$task->description}}</p>
    <p>{{ __('front.teams') }}</p>
    <ul>
      @forelse ($task->teams as $tt )
       <li><a href="{{route('detailTeamUsers',$tt->id)}}">{{ $tt->name }}</a></a>
          <ul>
           @forelse ($tt->users as $tu )
           <li>{{$tu->name}} {{($tu->pivot->leader)? 'Lider' :''}}</li>
           @empty
            
           @endforelse
          </ul>
       </li>
      @empty
       <li>Ningun equipo </li>
      @endforelse
    </ul>
    @if ($task->file)
       <p style="color: red; "><a href="{{URL::asset('storage'.$task->file)}}" target="_blank" class="text-center bg-red">{{ __('front.download') }}</a></p>
    @endif
    <a href="{{route('home')}}" class="btn btn-primary">{{ __('front.back') }}</a>
  </div>
</div>
@endsection
@section('scripts')
@endsection
