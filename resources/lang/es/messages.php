<?php
return [
 'error'=>'Error al insertar registro',
 'save'=>'Guardado correctamente',
 'update'=>'Actualizado correctamente',
 'nameRequired'=>'Nombre requerido',
 'nameUnique'=>'Nombre unico, ya existe!!',
 'descriptionRequired'=>'Description requerido',
 'phoneRequired'=>'Teléfono requerido',
 'addressRequired'=>'Dirección requerido',
 'fileRequired'=>'Archivo pdf requerido',
 'emailUnique'=>'Email unico, ya existe!!!',
 'emailRequired'=>'Email requerido',
 'emailEmail'=>'Email debe ser de tipo email',
 'delete'=>'Eliminado correctamente',
 'noFound'=>'Registro no encontrado',
 'exists'=>'Usuario ya registrado a este registro'
];