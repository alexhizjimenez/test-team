const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
  .js("resources/js/app.js", "public/js")
  .js("resources/js/messages/messages.js", "public/js/messages/messages.js")
  .js("resources/js/tasks/tasksActions.js", "public/js/tasks/tasksActions.js")
  .js("resources/js/teams/teamsActions.js", "public/js/teams/teamsActions.js")
  .js("resources/js/users/usersActions.js", "public/js/users/usersActions.js")
  .sass("resources/sass/app.scss", "public/css")
  .sourceMaps();
