<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'name'=>'required|unique:tasks,name,'.$this->id.',id',
            'description'=>'required',
        ];
    }
    public function messages()
    {
        return [
            'name.unique'=>__('messages.nameUnique'),
            'name.required'=>__('messages.nameRequired'),
            'description.required'=>__('messages.descriptionRequired'),
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'msg' =>$validator->errors(),
            'data' => null,
        ]));
    }
}
