<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'email'=>'required|email|unique:users,email',
            'phone'=>'required',
            'address'=>'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>__('messages.nameRequired'),
            'email.required'=>__('messages.emailRequired'),
            'email.email'=>__('messages.emailEmail'),
            'email.unique'=>__('messages.emailUnique'),
            'phone.required'=>__('messages.phoneRequired'),
            'address.required'=>__('messages.addressRequired'),
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'msg' =>$validator->errors(),
            'data' => null,
        ]));
    }
}
