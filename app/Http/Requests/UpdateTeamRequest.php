<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateTeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'name'=>'required|unique:teams,name,'.$this->id.',id',
        ];
    }

    public function messages()
    {
        return [
            'name.unique'=>__('messages.nameUnique'),
            'name.required'=>__('messages.nameRequired'),
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'msg' =>$validator->errors(),
            'data' => null,
        ]));
    }
}
