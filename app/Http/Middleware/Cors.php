<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        return $next($request)
        //Url a la que se le dará acceso en las peticiones
       ->header("access-control-allow-origin", "*")
       //Métodos que a los que se da acceso
       ->header("access-control-allow-methods", "GET, POST, PUT, DELETE")
       //Headers de la petición
       ->header("access-control-allow-headers", "X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method"); 
 
    }
}
