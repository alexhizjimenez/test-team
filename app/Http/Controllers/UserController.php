<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
   
    public function index()
    {
        $users = User::where('is_admin','=',0)->latest()->paginate(10);
        return view('users',compact('users'));
    }

   
}
