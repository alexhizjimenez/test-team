<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTeamRequest;
use App\Http\Requests\UpdateTeamRequest;
use App\Models\Team;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::all();
        return response()->json($teams);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTeamRequest $request)
    {
        Team::create($request->all());
        return response()->json(['success' => true, 'msg' => __('messages.save')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTeamRequest $request, $id)
    {
        Team::where('id', '=', $id)->update($request->all());
        return response()->json(['success' => 'true', 'msg' => __('messages.update')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team = Team::find($id);
        if (!$team)
            return response()->json(['success' => false, 'msg' => [__('messages.noFound')]]);
        $team->delete();
        return response()->json(['success' => true, 'msg' => __('messages.delete')]);
    }

    public function asignarUserToTeam(Request $request)
    {
        $team = Team::find($request->team_id);
        $user = User::find($request->user_id);
        if (!$team)
            return response()->json(['success' => false, 'msg' => [__('messages.noFound')]]);
        if (!$user)
            return response()->json(['success' => false, 'msg' => [__('messages.noFound')]]);
        $id = $request->user_id;
        $countTeam = $team->users()->wherePivot('user_id', $id)->get()->count();
        $countUser = $user->teams()->where('user_id', $id)->count();
        if ($countUser > 0 || $countTeam > 0)
            return response()->json(['success' => false, 'msg' => ['error' => __('messages.exists')]]);

        $team->users()->attach($id);
        return response()->json(['success' => true, 'msg' => __('messages.save')]);
    }

    public function deleteRelationUserToTeam($id, $idTeam)
    {
        $team = Team::find($idTeam);
        if (!$team)
            return response()->json(['success' => false, 'msg' => [__('messages.noFound')]]);
        $team->users()->detach($id);
        return response()->json(['success' => true, 'msg' => __('messages.delete')]);
    }

    public function toAssignLeaderUser(Request $request, $id)
    {
        $team = Team::find($id);
        if (!$team)
            return response()->json(['success' => false, 'msg' => [__('messages.noFound')]]);
        $ids = $team->users()->pluck('user_id');
        $team->users()->syncWithPivotValues($ids, ['leader' => 0], false);
        $team->users($request->id)->sync([$request->id => ['leader' => 1]], false);
        return response()->json(['success' => true, 'msg' => __('messages.update')]);
        
    }
}
