<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Task;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::all();
        return response()->json($tasks);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTaskRequest $request)
    {
        $data = $request->all();
        
        if ($request->hasFile('file')) {
            $doc = $request->file('file');
            $nameReplaceDocument = str_replace(' ', '', $doc->getClientOriginalName());
            $nameDoc = time() . $nameReplaceDocument;
            $data['file'] = '/tasks-documents/documents/' . $nameDoc;
            $doc->move(public_path('storage') . '/tasks-documents/documents/', $nameDoc);
        }
        
        Task::create($data);
        return response()->json(['success' => true, 'msg' => __('messages.save')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTaskRequest $request, $id)
    {
        /*
        $data = $request->all();
        
        if ($request->hasFile('file')) {
            $doc = $request->file('file');
            $nameReplaceDocument = str_replace(' ', '', $doc->getClientOriginalName());
            $nameDoc = time() . $nameReplaceDocument;
            $data['file'] = '/tasks-documents/documents/' . $nameDoc;
            $doc->move(public_path('storage') . '/tasks-documents/documents/', $nameDoc);
        }
        $task = Task::where('id', '=', $id)->update($data);
        $image_path = public_path('storage') . $task->file;
        if (File::exists($image_path)) File::delete($image_path);
        return response()->json(['success' => true, 'msg' => 'Editado correctamente']);
        */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);
        if (!$task)
            return response()->json(['success' => false, 'msg' => __('messages.noFound')]);
        $image_path = public_path('storage') . $task->file;
        if (File::exists($image_path)) File::delete($image_path);
        $task->delete();
        return response()->json(['success' => true, 'msg' => __('messages.delete')]);
    }

    public function asignarTeamToTask(Request $request)
    {
        $team = Team::find($request->team_id);
        $task = Task::find($request->task_id);
        if (!$team)
            return response()->json(['success' => false, 'msg' => 'Registro equipo no encontrado']);
        if (!$task)
            return response()->json(['success' => false, 'msg' => 'Registro tarea no encontrado']);
        $id = $request->team_id;
        $countTeam = $task->teams()->wherePivot('team_id', $id)->get()->count();
        if ($countTeam > 0)
            return response()->json(['success' => false, 'msg' => ['error' => "Error equipo ya esta registrado a esta tarea "]]);

        $task->teams()->attach($id);
        return response()->json(['success' => true, 'msg' => 'Agregado correctamente']);
    }

    public function deleteRelationTeamToTask($id, $idTask)
    {
        $task = Task::find($idTask);
        if (!$task)
            return response()->json(['success' => false, 'msg' => ['Registro no encontrado']]);
        $task->teams()->detach($id);
        return response()->json(['success' => true, 'msg' => 'Eliminado']);
    }


    public function updateTask(UpdateTaskRequest $request){
        $task = Task::find($request->id);
        $image_path = public_path('storage') . $task->file;
        if (File::exists($image_path)) File::delete($image_path);
        $data = $request->except(['id']);
        
        if ($request->hasFile('file')) {
            $doc = $request->file('file');
            $nameReplaceDocument = str_replace(' ', '', $doc->getClientOriginalName());
            $nameDoc = time() . $nameReplaceDocument;
            $data['file'] = '/tasks-documents/documents/' . $nameDoc;
            $doc->move(public_path('storage') . '/tasks-documents/documents/', $nameDoc);
        }
        Task::where('id', '=', $request->id)->update($data);
        
        return response()->json(['success' => true, 'msg' => __('messages.update')]);
    }

    
}
