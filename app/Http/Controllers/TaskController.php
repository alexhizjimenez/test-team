<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Team;
use App\Models\User;

class TaskController extends Controller
{
    
    public function index()
    {
        $tasks = Task::latest()->paginate(10);
        $teams = Team::latest()->get();
        return view('tasks',compact(['tasks','teams']));
    }

    public function edit($id){
        $task = Task::find($id);
        return view('editTask',compact('task'));
    }

    
    
}
