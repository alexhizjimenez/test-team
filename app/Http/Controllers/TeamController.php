<?php

namespace App\Http\Controllers;

use App\Models\Team;
use App\Http\Requests\StoreTeamRequest;
use App\Http\Requests\UpdateTeamRequest;
use App\Models\User;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    public function index()
    {
        $teams = Team::latest()->paginate(10);
        $users = User::latest()->get();
        return view('teams',compact(['teams','users']));
    }

    public function edit($id){
        $team = Team::find($id);
        return view('editTeam',compact('team'));
    }

    

    
}
