<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\Team;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tasks = Task::all();
        return view('welcome',compact('tasks'));
        
    }

    

    public function show($id){
        $task = Task::find($id);
        return view('detailTaskTeam',compact('task'));
    }

    public function showTeamUsers($id){
        $team = Team::find($id);
        return view('detailTeamUsers',compact('team'));
    }

    public function usersToTeam(){
        $users = User::latest()->paginate(20);
        return view('usersToTeam', compact('users'));
    }
}
