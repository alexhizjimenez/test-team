<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create([
            'name'=>'alexis jimenez',
            'email'=>'ale@gmail.com',
            'password'=>bcrypt('admin'),
            'is_admin'=>true,
        ]);
        User::factory(15)->create();
    }
}
